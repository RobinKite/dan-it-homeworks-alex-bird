const btn = document.querySelector('.burger-menu');
const menu = document.querySelector('.header__nav');
btn.addEventListener('click', () => {
	btn.classList.toggle('btn--close');
	menu.classList.toggle('menu--opened');
});
