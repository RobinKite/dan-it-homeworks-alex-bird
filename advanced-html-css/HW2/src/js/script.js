const btn = document.querySelector('.burger-menu');
const menu = document.querySelector('.nav-list');
btn.addEventListener('click', () => {
	btn.classList.toggle('open-menu');
	menu.classList.toggle('menu-open');
});
