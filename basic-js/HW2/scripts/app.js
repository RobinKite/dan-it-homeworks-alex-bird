let userName;
let userAge;
const adultMinAge = 18;
const adultMaxAge = 22;

while (!userName || !userAge || isNaN(userAge)) {
    userName = prompt('Please, enter your name', userName);
    userAge = prompt('Please, enter your age', userAge);
    
    if (!userName && !userAge) {
        alert('Error, fields are empty')
    } else if (!userName) {
        alert('Error, name field is empty')
        // Ім'я потрібно зробити пустим, щоб наступного разу не було присвоєне null
        userName = '';
    } else if (!userAge) {
        alert('Error, age field is empty')
        // Так само і тут
        userAge = '';
    } else if (isNaN(userAge)) {
        alert('Error, age is not in digital form')
    }
}

if (parseInt(userAge) < adultMinAge) {
    alert('You are not allowed to visit this website');
} else if (parseInt(userAge) >= adultMinAge && parseInt(userAge) <= adultMaxAge) {
    let ageConfirm = confirm('Are you sure you want to continue?');
    if (ageConfirm) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${userName}`);
}