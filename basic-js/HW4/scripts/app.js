function calcNumbers() {
    const numbers = getNumbers();
    const op = getOperation();

    console.log(`${numbers[0]} ${op} ${numbers[1]} =`);    

    switch (op){
        case "+":
            return numbers[0] + numbers[1];
        case "-":
            return numbers[0] - numbers[1];
        case "/":
            return numbers[0] / numbers[1];
        case "*":
            return numbers[0] * numbers[1];
        default:
            console.warn(`Attention, calc can't proceed!`);
            return;
    } 

    function getOperation(){
        const z = prompt('Please, enter mathematical operation', arguments[0])
        if (checkOperation(z)) {
            return z;
        } else {
            return getOperation(z);
        }
    }

    function getNumbers() {
        const x = prompt('Please, enter your first number', arguments[0]);
        const y = prompt('Please, enter your second number', arguments[1]);
        if (checkNumbers(x,y)) {
            return [parseFloat(x),parseFloat(y)];        
        } else {
            return getNumbers(x,y);
        }
    }

    function checkNumbers(x, y) {
        if (isNaN(x) || isNaN(y)) {
            console.warn('Attention, numbers must be in digital form!');
            return false;
        } else {
            return true;
        }
    }
    function checkOperation(z) {
        if (z === '' || z.length > 1) {
            console.warn(`Attention, operation ${z} is incorrect!`);
            return false;
        } else if (z === '+' || z === '-' || z === '*' || z === '/'){
            return true;
        } else {
            console.warn(`Attention, only +, -, *, / are allowed!`);
            return false;
        }
    }
}

console.log(calcNumbers());