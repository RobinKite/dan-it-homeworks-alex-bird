// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.

const arr = ['hello', 'world', 23, '23', null, true];
const allTypes = ['string', 'number', 'boolean', 'null'];
allTypes.forEach((type) => console.log(filterBy(arr, type)));

function filterBy(array, type) {
  return array.filter(
    (item) =>
      (typeof item !== type && type !== 'null') ||
      (item !== null && type === 'null')
  );
}

//АБО

// function filterBy(array, type) {
//   const filteredArray = [];
//   array.forEach((element) => {
//     if (
//       (typeof element !== type && type !== 'null') ||
//       (element !== null && type === 'null')
//     ) {
//       filteredArray.push(element);
//     }
//   });
//   return filteredArray;
// }
