//1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

//2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

//3 Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

//4 Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

//5 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

//TASK 1
const allPs = document.querySelectorAll('p');
allPs.forEach((p) => (p.style.backgroundColor = '#ff0000'));

//TASK 2
const options = document.getElementById('optionsList');
console.log(options);
console.log(options.parentElement);
console.log(options.childNodes);

//TASK 3 (такого класу не існує, тому шукаю з id)
const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';

//TASK 4
const header = document.querySelector('.main-header');
for (const child of header.children) {
  console.log(child);
  child.classList.add('nav-item');
}

//TASK 5
const titles = document.querySelector('.section-title');
titles.classList.remove('section-title');
