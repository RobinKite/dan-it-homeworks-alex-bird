const menuButton = document.querySelector(".header-menu__button");
const menuHeader = document.querySelector(".page-nav__list");
let checkMenuHeaderPressed = false;

menuButton.addEventListener("click", (e) => {
  if (!checkMenuHeaderPressed) {
    Array.from(menuButton.children).forEach((span, i) => {
      span.style.position = "absolute";
      span.style.top = "50%";
      span.style.left = "0";
      span.style.transform = "translateY(-50%)";
      span.style.transition = "all 0.3s ease-in";
      if (i === 0) span.style.transform = "rotate(45deg)";
      if (i === 1) span.style.transform = "scale(0)";
      if (i === 2) span.style.transform = "rotate(-45deg)";
    });
    checkMenuHeaderPressed = true;
    menuHeader.style.transform = "translateX(0)";
  } else {
    Array.from(menuButton.children).forEach((span, i) => {
      span.style.removeProperty("position");
      span.style.removeProperty("top");
      span.style.removeProperty("left");
      span.style.removeProperty("transform");
    });
    checkMenuHeaderPressed = false;
    menuHeader.style.removeProperty("transform");
  }
});

const servMenuButton = document.querySelector(".services-menu__button");

servMenuButton.addEventListener("click", (e) => {
  servMenuList.dataset.menu =
    servMenuList.dataset.menu === "closed" ? "opened" : "closed";
});
