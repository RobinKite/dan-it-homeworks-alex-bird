const elem = document.querySelector(".grid");
let iso = new Masonry(elem, {
  itemSelector: ".grid-item",
});

const galleryAlbum = [
  "./images/gallery/photo-1.jpg",
  "./images/gallery/photo-2.jpg",
  "./images/gallery/photo-3.jpg",
  "./images/gallery/photo-4.jpg",
  "./images/gallery/photo-5.jpg",
  "./images/gallery/photo-6.jpg",
  "./images/gallery/photo-7.jpg",
];

const galleryList = document.querySelector(".gallery__list");
const gridLoadMore = galleryList.parentElement.querySelector(".load-more");

gridLoadMore.addEventListener("click", gridLoadMoreAction);

function gridLoadMoreAction(e) {
  e.preventDefault();

  const template = document.querySelector("[data-gallery-item-template]");

  galleryAlbum.forEach((el) => {
    createGalleryItem(galleryList, el, template);
  });
}

function createGalleryItem(parent, el, template) {
  const li = template.content.cloneNode(true).children[0];
  const img = li.querySelector(".gallery__photo");
  gridLoadMore.disabled = true;

  parent.append(li);

  showLoading(el, img, gridLoadMore, parent, false);

  setTimeout(() => {
    gridLoadMore.remove();

    iso = new Masonry(elem, {
      itemSelector: ".grid-item",
    });
  }, 2000);
}
