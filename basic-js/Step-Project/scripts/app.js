const workAlbum = {
  "graphic design": [
    "./images/work/graphic design/graphic-design1.jpg",
    "./images/work/graphic design/graphic-design2.jpg",
    "./images/work/graphic design/graphic-design3.jpg",
    "./images/work/graphic design/graphic-design4.jpg",
    "./images/work/graphic design/graphic-design5.jpg",
    "./images/work/graphic design/graphic-design1.jpg",
    "./images/work/graphic design/graphic-design2.jpg",
    "./images/work/graphic design/graphic-design3.jpg",
    // "./images/work/graphic design/graphic-design4.jpg",
    // "./images/work/graphic design/graphic-design5.jpg",
  ],
  "landing pages": [
    "./images/work/landing page/landing-page1.jpg",
    "./images/work/landing page/landing-page2.jpg",
    "./images/work/landing page/landing-page3.jpg",
    "./images/work/landing page/landing-page4.jpg",
    "./images/work/landing page/landing-page5.jpg",
    "./images/work/landing page/landing-page1.jpg",
    "./images/work/landing page/landing-page2.jpg",
    "./images/work/landing page/landing-page3.jpg",
    // "./images/work/landing page/landing-page4.jpg",
    // "./images/work/landing page/landing-page5.jpg",
  ],
  "web design": [
    "./images/work/web design/web-design1.jpg",
    "./images/work/web design/web-design2.jpg",
    "./images/work/web design/web-design3.jpg",
    "./images/work/web design/web-design4.jpg",
    "./images/work/web design/web-design5.jpg",
    "./images/work/web design/web-design6.jpg",
    "./images/work/web design/web-design1.jpg",
    "./images/work/web design/web-design2.jpg",
    // "./images/work/web design/web-design3.jpg",
    // "./images/work/web design/web-design4.jpg",
    // "./images/work/web design/web-design5.jpg",
    // "./images/work/web design/web-design6.jpg",
  ],
  wordpress: [
    "./images/work/wordpress/wordpress1.jpg",
    "./images/work/wordpress/wordpress2.jpg",
    "./images/work/wordpress/wordpress3.jpg",
    "./images/work/wordpress/wordpress4.jpg",
    "./images/work/wordpress/wordpress5.jpg",
    "./images/work/wordpress/wordpress6.jpg",
    "./images/work/wordpress/wordpress1.jpg",
    "./images/work/wordpress/wordpress2.jpg",
    // "./images/work/wordpress/wordpress3.jpg",
    // "./images/work/wordpress/wordpress4.jpg",
    // "./images/work/wordpress/wordpress5.jpg",
    // "./images/work/wordpress/wordpress6.jpg",
  ],
};

const usersFeedback = [
  {
    id: 1,
    name: "Hasan Ali",
    position: "UX designer",
    feedback: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.`,
    photo: "./images/clients/img-01.jpg",
  },
  {
    id: 2,
    name: "Ervin Howell",
    position: "IT technician",
    feedback: `Announcing of invitation principles in. Cold in late or deal. Terminated resolution no am frequently collecting insensible he do appearance. Projection invitation affronting admiration if no on or. It as instrument boisterous frequently apartments an in.`,
    photo: "./images/clients/img-03.jpg",
  },
  {
    id: 3,
    name: "Clementine Bauch",
    position: "Support specialist",
    feedback: `Mr excellence inquietude conviction is in unreserved particular. You fully seems stand nay own point walls. Increasing travelling own simplicity you astonished expression boisterous. Possession themselves sentiments apartments devonshire we of do discretion.`,
    photo: "./images/clients/img-04.jpg",
  },
  {
    id: 4,
    name: "Patricia Lebsack",
    position: "QA tester",
    feedback: `Smallest directly families surprise honoured am an. Speaking replying mistress him numerous she returned feelings may day. Evening way luckily son exposed get general greatly. Zealously prevailed be arranging do.`,
    photo: "./images/clients/img-05.jpg",
  },
  {
    id: 5,
    name: "Chelsey Dietrich",
    position: "Web developer",
    feedback: `Set arranging too dejection september happiness. Understood instrument or do connection no appearance do invitation. Dried quick round it or order. Add past see west felt did any.`,
    photo: "./images/clients/img-06.jpg",
  },
  {
    id: 6,
    name: "Mrs. Dennis Schulist",
    position: "IT security specialist",
    feedback: `Whether article spirits new her covered hastily sitting her. Money witty books nor son add. Chicken age had evening believe but proceed pretend mrs. At missed advice my it no sister. Miss told ham dull knew see she spot near can.`,
    photo: "./images/clients/img-07.jpg",
  },
  {
    id: 7,
    name: "Kurtis Weissnat",
    position: "Programmer",
    feedback: `Two exquisite objection delighted deficient yet its contained. Cordial because are account evident its subject but eat. Can properly followed learning prepared you doubtful yet him. Over many our good lady feet ask that.`,
    photo: "./images/clients/img-08.jpg",
  },
  {
    id: 8,
    name: "Nicholas Runolfsdottir V",
    position: "Systems analyst",
    feedback: `Expenses own moderate day fat trifling stronger sir domestic feelings. Itself at be answer always exeter up do. Though or my plenty uneasy do. Friendship so considered remarkably be to sentiments.`,
    photo: "./images/clients/img-09.jpg",
  },
  {
    id: 9,
    name: "Glenna Reichert",
    position: "Network engineer",
    feedback: `Offered mention greater fifteen one promise because nor. Why denoting speaking fat indulged saw dwelling raillery. Spirit her entire her called. Say out noise you taste merry plate you share. My resolve arrived is we chamber be removal.`,
    photo: "./images/clients/img-10.jpg",
  },
  {
    id: 10,
    name: "Clementina DuBuque",
    position: "Software engineer",
    feedback: `Eat imagine you chiefly few end ferrars compass. Be visitor females am ferrars inquiry. Latter law remark two lively thrown. Spot set they know rest its. Raptures law diverted believed jennings consider children the see. `,
    photo: "./images/clients/img-11.jpg",
  },
];

/* ------------------------------ SERVICE --------------------------------*/

const servMenuList = document.querySelector(".services-menu__list");
const servContentList = document.querySelector(".services-content__list");
const servMenuLinks = servMenuList.querySelectorAll(".services-menu__option");

let servLastActive = [
  document.querySelector(".services-menu__item[data-active]"),
  document.querySelector(".services-content__item[data-active]"),
];

createDatasets(
  Array.from(servMenuList.children),
  Array.from(servMenuLinks),
  "link"
);
createDatasets(
  Array.from(servContentList.children),
  Array.from(servMenuLinks),
  "link"
);

// LISTENERS
servMenuList.addEventListener("click", servItemAction);

// FUNCTIONS
function servItemAction(e) {
  e.preventDefault();

  if (
    e.target.parentNode.dataset.hasOwnProperty("link") &&
    !e.target.parentNode.dataset.hasOwnProperty("active")
  ) {
    const li = e.target.parentNode;
    const content = Array.from(servContentList.children);

    servLastActive.forEach((item) => item.removeAttribute("data-active"));

    li.dataset.active = true;
    servLastActive.length = 0;
    servLastActive.push(li);

    content.forEach((item) => {
      if (item.dataset.link === li.dataset.link) {
        item.dataset.active = "true";
        servLastActive.push(item);
      }
    });
  }
}

/* ------------------------------- WORK ----------------------------------*/

const workMenuList = document.querySelector(".work__list");
const workContentList = document.querySelector(".album__list");
const workMenuLinks = workMenuList.querySelectorAll(".work__link");
const workLoadMore = workMenuList.parentElement.querySelector(".load-more");
const workPhotosAmount = Object.values(workAlbum).reduce((acc, arr) => {
  return acc + arr.length;
}, 0);

createDatasets(
  Array.from(workMenuList.children),
  Array.from(workMenuLinks),
  "link"
);

Object.keys(workAlbum).forEach((item) => {
  renderPhotos(workAlbum[item], item, workContentList, 0);
});

// LISTENERS

workMenuList.addEventListener("click", workItemAction);

workLoadMore.addEventListener("click", workLoadMoreAction);

// FUNCTIONS

function workItemAction(e) {
  e.preventDefault();

  if (e.target.parentNode.dataset.hasOwnProperty("link")) {
    const li = e.target.parentNode;

    if (li.dataset.link === "all") {
      renderAll(workAlbum, workContentList, 0);
    } else {
      onSort(workAlbum[li.dataset.link], li.dataset.link, workContentList, 0);
    }
  }
}

function workLoadMoreAction(e) {
  e.preventDefault();

  let amount = Object.values(workAlbum).sort((a, b) => b.length - a.length)[0]
    .length;

  workMenuList.dataset.hasOwnProperty("amount") &&
  workMenuList.dataset.amount < amount
    ? (amount = parseInt(workMenuList.dataset.amount) + 3)
    : (amount = 3);
  workMenuList.dataset.amount = amount;

  renderAll(workAlbum, workContentList, amount);
}

function renderAll(obj, parent, amount) {
  if (amount === 0) clearParent(parent);
  Object.keys(obj).forEach((item) => {
    renderPhotos(obj[item], item, parent, amount);
  });
  Array.from(parent.children).length !== workPhotosAmount
    ? (workLoadMore.style.display = "flex")
    : (workLoadMore.style.display = "none");
}

function onSort(arr, name, parent, amount, type = "sort") {
  clearParent(parent);
  renderPhotos(arr, name, parent, amount, type);
  workLoadMore.style.display = "none";
}

function clearParent(parent) {
  parent.innerHTML = "";
}

function renderPhotos(list, name, parent, maxNumber, ...args) {
  const template = document.querySelector("[data-album-template]");
  const leftItems = list.slice(maxNumber);

  leftItems.forEach((element, i) => {
    if (!args.length && i < 3) {
      workcreateLi(template, element, name, parent);
    } else if (!!args.length) {
      workcreateLi(template, element, name, parent);
    }
  });
}

function workcreateLi(template, element, name, parent) {
  const li = template.content.cloneNode(true).children[0];
  const img = li.querySelector(".album__image");
  const p = li.querySelector(".album-card__text");

  li.dataset.link = name;

  img.alt = `${name} photo`;
  p.textContent = camelize(name);

  workLoadMore.disabled = true;

  parent.append(li);

  showLoading(element, img, workLoadMore, parent);
}

/* ----------------------------- FEEDBACK --------------------------------*/

const fdbckContentList = document.querySelector(".feedback__list");
const fdbckClientsList = document.querySelector(".clients__list");
const fdbckCarousel = document.querySelector(".carousel");

const itemWidth = 70;
const itemGap = 27;
let itemsAmount;
let fdbckLastActive = [];

renderFeedback(usersFeedback, fdbckContentList);
renderFBPhotos(usersFeedback, fdbckClientsList);

itemsAmount = fdbckClientsList.children.length;

if (Number.isInteger(usersFeedback.length / 2)) {
  fdbckClientsList.style.transform = `translateX(${
    (itemWidth + itemGap) / 2
  }px)`;
}

// LISTENERS

fdbckCarousel.addEventListener("click", carouselAction);

function carouselAction(e) {
  e.preventDefault();

  if (
    e.target.closest("button") &&
    e.target.closest("button").dataset.hasOwnProperty("arrow")
  ) {
    const btn = e.target.closest("button");
    if (btn.dataset.arrow === "right") {
      moveRight();
    }
    if (btn.dataset.arrow === "left") {
      moveLeft();
    }
  }

  if (
    (e.target.parentElement.dataset.hasOwnProperty("photoId") ||
      e.target.parentElement.dataset.hasOwnProperty("copy")) &&
    !e.target.parentElement.dataset.hasOwnProperty("active")
  ) {
    const li = document.activeElement;
    const currentIndex = Array.from(fdbckClientsList.children).indexOf(li);

    if (currentIndex === itemsAmount / 2) {
      moveRight();
    } else if (currentIndex === itemsAmount / 2 - 2) {
      moveLeft();
    }
  }
}

function moveRight() {
  const clone = fdbckClientsList.firstChild.cloneNode(true);
  const index = Math.ceil(itemsAmount / 2) - 1;

  fdbckClientsList.firstChild.remove();
  removeActive();
  addActive(index);
  fdbckClientsList.insertAdjacentElement("beforeend", clone);
}

function moveLeft() {
  const clone = fdbckClientsList.lastChild.cloneNode(true);
  const index = Math.ceil(itemsAmount / 2) - 2;

  fdbckClientsList.lastChild.remove();
  removeActive();
  addActive(index);
  fdbckClientsList.insertAdjacentElement("afterbegin", clone);
}

function removeActive() {
  fdbckLastActive.forEach((item) => item.removeAttribute("data-active"));
  fdbckLastActive.length = 0;
}

function addActive(i) {
  const client = Array.from(fdbckClientsList.children)[i];
  const content = Array.from(fdbckContentList.children).filter(
    (item) => item.dataset.photoId === client.dataset.photoId
  )[0];

  client.dataset.active = "true";
  content.dataset.active = "true";

  fdbckLastActive.push(content, client);
}

function renderFeedback(arrOfObjects, parent) {
  const template = document.querySelector("[data-feedback-profile-template]");

  arrOfObjects.forEach((obj) => {
    createFBContent(template, obj, parent);
  });
}

function renderFBPhotos(arrOfObjects, parent) {
  const template = document.querySelector("[data-feedback-photo-template]");
  arrOfObjects.forEach((obj, i, arr) => {
    createFBMenu(template, obj, i, arr, parent);
  });
}

function createFBMenu(template, obj, i, arr, parent) {
  const li = template.content.cloneNode(true).children[0];
  const img = li.querySelector(".clients__photo");

  if (fdbckLastActive.length === 1) {
    li.dataset.active = true;
    img.setAttribute("aria-label", "chosen client");
    fdbckLastActive.push(li);
  }

  li.dataset.photoId = obj.id;

  img.src = obj.photo;

  i > arr.length / 2 ? parent.prepend(li) : parent.append(li);
}

function createFBContent(template, obj, parent) {
  const li = template.content.cloneNode(true).children[0];
  const fb = li.querySelector(".feedback__content");
  const name = li.querySelector(".profile__name");
  const position = li.querySelector(".profile__position");
  const img = li.querySelector(".profile__image");

  if (!fdbckLastActive.length) {
    li.dataset.active = true;
    fdbckLastActive.push(li);
  }

  li.dataset.photoId = obj.id;

  fb.textContent = obj.feedback;
  name.textContent = obj.name;
  position.textContent = obj.position;
  img.src = obj.photo;

  parent.append(li);
}

/* ------------------------------- GLOBAL ---------------------------------*/

//! IF HEADER IS FIXED - CHANGE BG COLOR
// window.addEventListener("scroll", () => {
//   const y = (0.6 + window.pageYOffset / 3000).toFixed(2);
//   const header = document.querySelector(".page-nav__wrapper");

//   if (y < 1.05) header.style.backgroundColor = `rgba(94, 93, 88, ${y})`;

//   if (y >= 1) header.style.backgroundColor = `rgb(94, 93, 88)`;
// });

function camelize(str) {
  return str
    .split(" ")
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(" ");
}

function createDatasets(targetArr, valueArr, type) {
  targetArr.forEach(
    (el, i) =>
      (el.dataset[type] = valueArr[i].textContent
        ? valueArr[i].textContent.toLowerCase()
        : valueArr[i])
  );
}

function showLoading(element, img, button, parent, loaderPresent = true) {
  const loader = loaderPresent
    ? document.querySelectorAll("[data-loader]")
    : null;
  const dots = parent.parentElement.querySelector("[data-dots]");

  dots.style.display = "block";

  setTimeout(() => {
    img.src = element;

    button.disabled = false;
    dots.style.display = "none";

    if (loaderPresent) loader.forEach((l) => l.remove());
  }, 2000);
}
