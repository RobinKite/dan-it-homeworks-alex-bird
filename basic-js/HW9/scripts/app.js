// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body)
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

const cities = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const arr = ['1', '2', '3', 'sea', 'user', 23];
const complicatedArr = [
  'Kharkiv',
  'Kiev',
  ['Borispol', 'Irpin'],
  'Odessa',
  'Lviv',
  'Dnieper',
];

runTimer();
createList(cities);
createList(arr);
createList(complicatedArr);

function createList(array, parent = document.body) {
  const ul = document.createElement('ul');
  parent.append(ul);

  array.forEach((item) => {
    if (Array.isArray(item)) {
      createList(item, ul);
    }
    const li = document.createElement('li');
    li.innerText = item;
    ul.append(li);
  });
}

function runTimer() {
  let currentTime = 3;

  const timer = document.createElement('div');
  document.body.append(timer);
  timer.innerText = `${currentTime} seconds to full page clear`;

  const interval = setInterval(() => {
    currentTime -= 1;
    currentTime > 1
      ? (timer.innerText = `${currentTime} seconds to full page clear`)
      : (timer.innerText = `${currentTime} second to full page clear`);
  }, 1000);

  setTimeout(() => {
    clearInterval(interval);
    clearHTML();
  }, 3000);
}

function clearHTML() {
  document.body.innerHTML = '';
}
