function createNewUser() {
  const newUser = {};
  Object.defineProperties(newUser, {
    firstName: {
      value: prompt('Please, enter your name'),
      writable: false,
      configurable: true,
    },
    lastName: {
      value: prompt('Please, enter your surname'),
      writable: false,
      configurable: true,
    },
    birthDate: {
      value: checkBirth(prompt('Please, enter your birth date')),
      writable: false,
      configurable: true,
    },
    getAge: {
      value: function () {
        const currDay = new Date();
        const bDay = new Date(this.birthDate);
        const monthDiff = currDay.getMonth() - bDay.getMonth();
        let age = currDay.getFullYear() - bDay.getFullYear();
        if (
          monthDiff < 0 ||
          (monthDiff === 0 && currDay.getDate() < bDay.getDate())
        ) {
          age--;
        }
        return age;
      },
    },
    getLogin: {
      value: function () {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      },
    },
    getPassword: {
      value: function () {
        const year = new Date(this.birthDate);
        return (
          this.firstName[0].toUpperCase() +
          this.lastName.toLowerCase() +
          year.getFullYear()
        );
      },
    },
    setFirstName: {
      value: function (fName) {
        Object.defineProperty(newUser, 'firstName', { value: fName });
      },
    },
    setLastName: {
      value: function (lName) {
        Object.defineProperty(newUser, 'lastName', { value: lName });
      },
    },
    setBirthDate: {
      value: function (bDate) {
        Object.defineProperty(newUser, 'birthDate', { value: bDate });
      },
    },
  });

  function checkBirth(value) {
    while (!validateDate(value)) {
      value = prompt('Please, enter your birth date in a format dd.mm.yyyy');
    }
    return correctDate(value);
  }

  function validateDate(value) {
    const dateArr = value.split('.');
    dateArr[1] -= 1;
    const date = new Date(dateArr[2], dateArr[1], dateArr[0]);
    if (
      date.getFullYear() == dateArr[2] &&
      date.getMonth() == dateArr[1] &&
      date.getDate() == dateArr[0]
    ) {
      return true;
    } else {
      console.warn(`Incorrect Date: ${value}`);
      return false;
    }
  }

  function correctDate(value) {
    value = value.split('.');
    value = new Date(Date.UTC(value[2], value[1] - 1, value[0]));
    return value;
  }

  return newUser;
}

// Test
const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
