// !Завдання:
// Реалізувати функцію створення об'єкта "юзер".

// ?Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

// !Необов'язкове завдання підвищеної складності:
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

function createNewUser() {    
    const newUser = {};
    Object.defineProperties(newUser, {
        firstName: {
            value: prompt('Please, tell me your name'),
            writable: false,
            configurable: true,
        },
        lastName: {
            value: prompt('Please, tell me your surname'),
            writable: false,
            configurable: true,
        },
        getLogin: {            
            value: function () {
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
            }, 
        }, 
        setFirstName: {
            value: function (fName) {
            Object.defineProperty(newUser, 'firstName', {value: fName,});
            },    
        },
        setLastName: {
            value: function (lName) {
            Object.defineProperty(newUser, 'lastName', {value: lName,});
            },    
        },
    });
    return newUser;
}

// Test
const user = createNewUser();
console.log(user.getLogin());
user.firstName = 'John';
user.lastName = 'Brown';
console.log(user);
user.setFirstName('John');
user.setLastName('Brown');
console.log(user);