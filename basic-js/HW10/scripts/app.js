// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

const tabs = document.querySelector('.tabs');
tabs.addEventListener('click', tabActions);

const tabsTitles = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelector('.tabs-content');

let lastActiveChampion;

setDataForTabs(tabsTitles, tabsContent.children);

function setDataForTabs(titles, content) {
  for (let i = 0; i < titles.length; i++) {
    titles[i].dataset.champion = titles[i].textContent;
    content[i].dataset.champion = titles[i].textContent;

    if (!titles[i].classList.contains('active')) {
      content[i].style.display = 'none';
    } else {
      lastActiveChampion = titles[i].dataset.champion;
    }
  }
}

function tabActions(ev) {
  if (!ev.target.classList.contains('active')) {
    onPickTab(ev.target.dataset.champion);
  }
}

function onPickTab(champion) {
  const currentChampion = document.querySelectorAll(
    `[data-champion~='${champion}']`
  );
  const previousChampion = document.querySelectorAll(
    `[data-champion~='${lastActiveChampion}']`
  );

  currentChampion[0].classList.add('active');
  currentChampion[1].style.display = 'block';

  previousChampion[0].classList.remove('active');
  previousChampion[1].style.display = 'none';

  lastActiveChampion = champion;
}
