// Написати реалізацію кнопки "Показати пароль".
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
//? Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись

const form = document.forms[0];

form.addEventListener('click', (e) => {
  e.preventDefault();

  if (e.target.dataset.check === 'visibility') {
    const input = e.target.previousElementSibling;
    if (input.getAttribute('type') === 'password') {
      input.setAttribute('type', 'text');
    } else {
      input.setAttribute('type', 'password');
    }
    e.target.classList.toggle('fa-eye-slash');
  }

  if (e.target.dataset.check === 'compare-passwords') {
    const inputs = form.querySelectorAll('[data-input="password"]');
    const checkError = document.querySelector('[data-value="error"]');
    if (compare(...inputs)) {
      if (checkError) checkError.remove();
      for (const input of inputs) input.value = '';

      alert('Passwords are equal.'); //В завданні - 'You are welcome'?
    } else if (!checkError) {
      const errorText = document.createElement('div');
      errorText.dataset.value = 'error';
      errorText.textContent = 'Passwords must be equal!';
      errorText.style.color = 'red';
      e.target.insertAdjacentElement('beforebegin', errorText);
    }
  }
});

function compare(a, b, ...args) {
  if (args.length > 0) throw new Error(`Can compare only two elements`);
  if (!a.value || !b.value) return;
  return a.value === b.value;
}
