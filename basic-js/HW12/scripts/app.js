// Реалізувати функцію підсвічування клавіш.

// Технічні вимоги:
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

const wrapper = document.querySelectorAll('.btn');
const keysArr = Array.from(
  wrapper,
  ({ dataset, textContent }) => (dataset.id = textContent.trim().toLowerCase())
).filter(Boolean);

let lastPressed = null;

document.addEventListener('keydown', (e) => {
  const pressedKey = e.key.toLowerCase();
  if (keysArr.includes(pressedKey)) {
    if (lastPressed)
      wrapper[keysArr.indexOf(lastPressed)].style.backgroundColor = 'black';

    wrapper[keysArr.indexOf(pressedKey)].style.backgroundColor = 'blue';
    lastPressed = pressedKey;
  }
});
