// Реалізувати функцію, яка дозволить оцінити, чи команда розробників встигне здати проект до настання дедлайну.

// Технічні вимоги:
// Функція на вхід приймає три параметри:
// масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді. Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
// масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі. Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// Дата дедлайну (об'єкт типу Date).
// Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// Робота триває по 8 годин на день по будніх днях

console.log(calculateDeadline([6, 5, 4], [1000, 2000, 1500], '3.25.2023'));

function calculateDeadline(team, tasks, deadlineDate) {
  const startDate = new Date();
  const endDate = new Date(deadlineDate);
  const numOfDays = getBusinessDatesCount(startDate, endDate);
  const workPerHour = team.reduce((acc, item) => acc + item, 0);
  const hoursInDay = 8;
  const totalSP = workPerHour * hoursInDay * numOfDays; // in teory how much team can do story points till deadline
  const taskSP = tasks.reduce((acc, item) => acc + item, 0); // total story points needed to finish the work

  console.log(
    `Дата дедлайну: ${endDate}.\nКількість робочих днів до дедлайну: ${numOfDays}.\nКількість story points команда виконає до дедлайну: ${totalSP}.\nКількість story points потрібно виконати: ${taskSP}.`
  );

  if (totalSP < taskSP) {
    return `Команді розробників доведеться витратити додатково ${Math.ceil(
      (taskSP - totalSP) / workPerHour
    )} годин після дедлайну, щоб виконати всі завдання в беклозі`;
  } else {
    return `Усі завдання будуть успішно виконані за ${Math.floor(
      (totalSP - taskSP) / (workPerHour * hoursInDay)
    )} днів до настання дедлайну!`;
  }
}
function getBusinessDatesCount(startDate, endDate) {
  let count = 0;
  const curDate = new Date(startDate.getTime());
  while (curDate <= endDate) {
    const dayOfWeek = curDate.getDay();
    if (dayOfWeek !== 0 && dayOfWeek !== 6) count++;
    curDate.setDate(curDate.getDate() + 1);
  }
  return count;
}
