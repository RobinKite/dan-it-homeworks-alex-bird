// Технічні вимоги:
//// В архіві дана верстка макету калькулятора. Необхідно зробити цей калькулятор робочим.
//// При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
//// При кліку на знаки операторів (*, /, +, -) на екрані нічого не відбувається - програма чекає на введення другого числа для виконання операції.
//// Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки =, так і будь-якого з операторів, у табло повинен з'явитися результат виконання попереднього виразу.
// При натисканні клавіш M+ або M- у лівій частині табло необхідно показати маленьку букву m - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання MRC має очищати пам'ять.
//// Необов'язкове завдання підвищеної складності
//// Усі клавіші калькулятора також повинні натискатися з клавіатури. Натискання Enter буде різнозначним натисканню =

calculator();

function calculator() {
	const box = document.querySelector('.box');
	const display = document.querySelector('.display input');
	const operators = ['/', '*', '-', '+'];

	let firstValue = 0;
	let currentOperator;
	let secondValue = 0;

	//Creating data-id for each input
	const inputs = box.querySelectorAll('[value]');
	inputs.forEach((input) => {
		input.dataset.id = input.value;
	});

	//Listeners
	box.addEventListener('click', (e) => {
		if (e.target.dataset.hasOwnProperty('id')) {
			const target = e.target.dataset.id;

			if (target === 'm+' && display.value) {
				createM();
				localStorage.setItem('memory', display.value);
				display.dataset.pressedMRC = 'false';
			}

			if (target === 'm-' && localStorage.getItem('memory')) {
				removeM();
				if (display.dataset.pressedMRC)
					delete display.dataset.pressedMRC;
				localStorage.removeItem('memory');
			}

			if (target === 'mrc') {
				if (
					!display.dataset.pressedMRC &&
					display.value &&
					localStorage.getItem('memory')
				)
					display.dataset.pressedMRC = 'false';

				if (
					display.dataset.pressedMRC === 'true' &&
					display.value === localStorage.getItem('memory')
				) {
					removeM();
					display.value = '';
					delete display.dataset.pressedMRC;
					firstValue = '';
					localStorage.removeItem('memory');
				}

				if (
					localStorage.getItem('memory') &&
					display.dataset.pressedMRC === 'false'
				) {
					createM();
					display.dataset.pressedMRC = 'true';
					display.value = localStorage.getItem('memory');
					firstValue
						? (secondValue = localStorage.getItem('memory'))
						: (firstValue = localStorage.getItem('memory'));
				}
			}

			if (!isNaN(target)) {
				onNumber(target);
			}

			if (target === 'C') {
				onC();
			}

			if (target === '.' && !display.value.includes('.')) {
				onDot(target);
			}

			if (!secondValue && operators.includes(target)) {
				onOperator(target);
			}

			if (
				(secondValue && operators.includes(target)) ||
				(secondValue && target === '=')
			) {
				solve(target);
			}
		}
	});

	document.addEventListener('keydown', (e) => {
		if (!isNaN(e.key)) {
			onNumber(e.key);
		}

		if (e.key === 'Backspace' || e.key === 'Delete') {
			e.preventDefault();
			onC();
		}

		if (e.key === '.' && !display.value.includes('.')) {
			onDot(e.key);
		}

		if (!secondValue && operators.includes(e.key)) {
			onOperator(e.key);
		}

		if (
			(secondValue && operators.includes(e.key)) ||
			(secondValue && e.key === '=') ||
			(secondValue && e.key === 'Enter')
		) {
			e.preventDefault();
			e.key === 'Enter' ? solve('=') : solve(e.key);
		}
	});

	//Functions
	function onNumber(t) {
		checkM();

		if (firstValue && currentOperator && !secondValue) {
			display.value = null;
		}

		display.value += t;

		!currentOperator ? (firstValue += t) : (secondValue += t);
	}

	function onOperator(t) {
		checkM();

		currentOperator = t;
	}

	function onDot(t) {
		checkM();

		display.value += t;

		!currentOperator ? (firstValue += t) : (secondValue += t);
	}

	function onC() {
		checkM();

		display.value = null;
		firstValue = 0;
		secondValue = 0;
		onOperator();
	}

	function solve(t) {
		checkM();

		switch (currentOperator) {
			case '/': {
				display.value =
					parseFloat(firstValue) / parseFloat(secondValue);
				break;
			}
			case '*': {
				display.value =
					parseFloat(firstValue) * parseFloat(secondValue);
				break;
			}
			case '+': {
				display.value =
					parseFloat(firstValue) + parseFloat(secondValue);
				break;
			}
			case '-': {
				display.value =
					parseFloat(firstValue) - parseFloat(secondValue);
				break;
			}
		}
		firstValue = display.value;
		secondValue = 0;

		t === '=' ? onOperator() : onOperator(t);
	}

	function createM(parent = document.querySelector('.display')) {
		if (!document.querySelector('.m')) {
			checkM();

			const memory = document.createElement('div');
			memory.textContent = 'm';
			memory.classList.add('m');
			parent.append(memory);
		}
	}

	function removeM() {
		checkM();

		document.querySelector('.m').remove();
	}

	function checkM() {
		if (display.dataset.pressedMRC) display.dataset.pressedMRC = 'false';
	}
}
