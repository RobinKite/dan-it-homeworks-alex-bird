// Реалізувати функцію підрахунку факторіалу числа.

const number = askFactorial();

function askFactorial() {
	let factorial = prompt('Please, enter your number');
	while (!factorial || isNaN(factorial) || factorial < 0) {
		factorial = prompt(
			'Please, enter your positive number',
			factorial
		);
	}
	return factorial;
}

// function findFactorial(num) {
//   if (num === 0) {
//     return 1;
//   } else {
//     return num * findFactorial(num - 1);
//   }
// }

// АБО

function findFactorial(num) {
	let result = 1;
	for (let i = num; i > 0; i--) {
		result *= i;
	}
	return result;
}
console.log(findFactorial(number));

function func(n) {
	if (n === 1) return 1;
	return func(n - 1) + 2;
}
