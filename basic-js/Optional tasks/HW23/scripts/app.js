// Технічні вимоги:
// При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price. Це поле буде служити для введення числових значень
// Поведінка поля має бути наступною:
// При фокусі на полі вводу - має з'явитися рамка зеленого кольору. У разі втрати фокусу вона пропадає.
// Коли прибрано фокус з поля - його значення зчитується, над полем створюється span, у якому має бути текст: Поточна ціна: ${значення з поля вводу}. Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля вводу забарвлюється у зелений колір.
// При натисканні на Х - span з текстом та кнопка X повинні бути видалені. Значення, введене у поле вводу, обнулюється.
// Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу – Please enter correct price. span з некорректним значенням при цьому не створюється.
// У папці img лежать приклади реалізації поля вводу та "span", які можна брати як приклад'

const wrapper = document.querySelector('.wrapper');
const input = document.getElementById('price');

input.addEventListener('focus', (e) => {
  e.target.style.outline = '2px solid green';
  e.target.style.color = '';
});
input.addEventListener('blur', (e) => {
  e.target.style.outline = null;

  const checkSpan = document.querySelector('[data-id="current-price"]');
  const checkError = document.querySelector('[data-id="error-message"]');
  const priceValue = `Current price: ${e.target.value}`;

  if (checkInput(e.target.value)) {
    if (checkError) checkError.remove();

    createSpan(wrapper, priceValue);

    e.target.style.color = 'green';
  } else if (!checkError && !checkInput(e.target.value)) {
    const div = createElem(
      'div',
      wrapper,
      'Please enter correct price!',
      'beforeend'
    );
    div.dataset.id = 'error-message';
  }
});

function checkInput(data) {
  if (!data) return;
  if (isNaN(data)) return;
  if (data < 0) return;
  return data;
}

function createSpan(parent, content) {
  const div = createElem('div', parent, null, 'beforebegin');
  const span = createElem('span', div, content, 'afterbegin');
  const btn = createElem('button', div, 'x', 'beforeend');

  div.addEventListener('click', (e) => {
    if (e.target.dataset.id === 'clear-button') {
      div.remove();
      input.value = '';
    }
  });

  btn.style.marginLeft = '7px';

  span.dataset.id = 'current-price';
  btn.dataset.id = 'clear-button';
}

function createElem(type, parent, content, place) {
  const elem = document.createElement(type);
  elem.textContent = content;
  parent.insertAdjacentElement(place, elem);

  return elem;
}
