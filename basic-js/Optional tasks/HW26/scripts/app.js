// Реалізувати слайдер на чистому Javascript.

// Технічні вимоги:
//// Створити HTML розмітку, що містить кнопки Previous, Next, та картинки (6 штук), які будуть перегортатися горизонтально.
//// На сторінці має бути видно лише одну картинку. Зліва від неї буде кнопка Previous, праворуч – Next.
//// Після натискання на кнопку Next - має з'явитися нова картинка, що випливає зі списку.
//// Після натискання на кнопку Previous - має з'явитися попередня картинка.
//// Слайдер може бути нескінченним, тобто. якщо на початку натиснути на кнопку Previous, то з'явиться остання картинка, а якщо натиснути на Next, коли видима - остання картинка, то буде видно першу картинку.
// Приклад роботи слайдера можна побачити тут (http://kenwheeler.github.io/slick/) (перший приклад).

const container = document.querySelector('.slider');
const slider = document.querySelector('.slider--list');
const items = document.querySelectorAll('.slider--item');

const itemWidth = parseInt(
	getComputedStyle(items[0]).width
);
const itemsAmount = items.length;

const itemsCopy = slider.cloneNode(true).children;

const firstCopy = itemsCopy[0];
const lastCopy = itemsCopy[itemsAmount - 1];

let offset = -itemWidth;
let counter = 1;

slider.insertAdjacentElement('afterbegin', lastCopy);
slider.insertAdjacentElement('beforeend', firstCopy);

firstCopy.dataset.copy = 'first';
lastCopy.dataset.copy = 'last';

slider.style.left = `${-itemWidth}px`;

container.addEventListener('click', (e) => {
	if (e.target.dataset.button === 'prev') {
		if (counter <= 0) return;
		slider.style.transition = 'all 1s ease-in-out';

		offset += itemWidth;
		if (offset > 0) offset = -(itemWidth * itemsAmount);
		counter--;
		slider.style.left = `${offset}px`;
	}
	if (e.target.dataset.button === 'next') {
		if (counter >= itemsAmount + 1) return;

		slider.style.transition = 'all 1s ease-in-out';

		offset -= itemWidth;
		if (offset < -(itemWidth * (itemsAmount + 1)))
			offset = 0;
		counter++;
		slider.style.left = `${offset}px`;
	}
});

slider.addEventListener('transitionend', () => {
	const allItems =
		document.querySelectorAll('.slider--item');

	if (allItems[counter].hasAttribute('data-copy')) {
		if (allItems[counter].dataset.copy === 'first') {
			slider.style.transition = 'none';

			counter = 1;
			offset = -itemWidth;
		}
		if (allItems[counter].dataset.copy === 'last') {
			slider.style.transition = 'none';

			counter = itemsAmount;
			offset = -(itemWidth * itemsAmount);
		}

		slider.style.left = `${offset}px`;
	}
});
