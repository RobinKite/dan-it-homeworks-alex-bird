// Технічні вимоги:
// При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
// При натисканні на кнопку "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При кліку на конкретне коло - це коло має зникати, у своїй порожнє місце заповнюватися, тобто інші кола зрушуються вліво.
// У вас може виникнути бажання поставити обробник події на кожне коло для його зникнення. Це неефективно, так не треба робити. На всю сторінку має бути лише один обробник подій, який це робитиме.

const controls = document.createElement('div');
controls.classList.add('controls');
const btn = document.createElement('button');
btn.textContent = 'Draw circle';
controls.append(btn);
document.body.insertAdjacentElement('beforeend', controls);

const WRAPPER_CLASS_NAME = 'circles-wrapper';
const CIRCLE_CLASS_NAME = 'circle';
const AMOUNT = 100;

btn.addEventListener('click', (e) => {
  const checkInput = document.querySelector(`[data-value="diameter"]`);

  if (!checkInput) {
    createInput('diameter');
  } else {
    const diameter = checkData(checkInput.value);
    drawCircles(AMOUNT, diameter);
  }
});

function createInput(dataName) {
  const input = document.createElement('input');
  input.dataset.value = dataName;
  input.setAttribute('placeholder', `Enter ${dataName}`);
  input.setAttribute('type', 'text');
  controls.append(input);
}

function checkData(data) {
  if (data && !isNaN(data)) return data;
}

function drawCircles(amount, d) {
  const checkWrapper = document.querySelector(`.${WRAPPER_CLASS_NAME}`);

  if (checkWrapper) {
    checkWrapper.remove();
  }

  const wrapper = createWrapper(document.body, WRAPPER_CLASS_NAME);

  for (let i = 1; i <= amount; i++) {
    createCircle(wrapper, i, d / 2);
  }
}

function setRadius(radius) {
  const root = document.querySelector(':root');
  if (root) root.style.setProperty('--radius', `${radius}px`);
}

function createRandomColor() {
  const hex = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += hex[Math.floor(Math.random() * 16)];
  }
  return color;
}

function createCircle(parent, i, r) {
  const circle = document.createElement('span');
  circle.dataset.count = i;
  circle.classList.add(CIRCLE_CLASS_NAME);
  setRadius(r);
  circle.style.backgroundColor = createRandomColor();
  parent.append(circle);
}

function createWrapper(parent, className) {
  const wrapper = document.createElement('div');
  wrapper.classList.add(className);
  parent.insertAdjacentElement('afterbegin', wrapper);
  wrapper.addEventListener('click', (e) => {
    // console.log(e.target);
    if (e.target.dataset.hasOwnProperty('count')) e.target.remove();
  });

  return wrapper;
}
