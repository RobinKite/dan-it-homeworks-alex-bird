// Створити об'єкт "студент" та проаналізувати його табель.

// Технічні вимоги:
// Створити порожній об'єкт student, з полями name та lastName.
// Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
// порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.

function Student(name, lastname) {
  (this.name = name), (this.lastname = lastname);
}

function createStudent() {
  const tabel = new Student();
  tabel.name = prompt("Please, enter student's name");
  tabel.lastname = prompt("Please, enter student's surname");
  createReportCard(tabel);
  return tabel;
}

console.log(createStudent());

function createReportCard(stud) {
  stud.subjects = {};

  while (true) {
    let subject = prompt('Please, enter a new Subject [Press Cancel to stop]');
    if (subject === null) {
      break;
    } else if (!isNaN(subject)) {
      subject = prompt('Please, enter a new Subject [Press Cancel to stop]');
    } else {
      const grade = prompt("Please, enter subject's grade");
      grade === null || isNaN(grade)
        ? (stud.subjects[subject] = 0)
        : (stud.subjects[subject] = parseFloat(grade));
    }
  }
  if (!checkTransfer(stud.subjects))
    console.log('The student has been transferred to the next course!');
  if (checkScholarship(stud.subjects) > 7)
    console.log('The student is awarded a scholarship!');
}

function checkTransfer(subj) {
  let count = 0;
  for (let grades in subj) {
    subj[grades] < 4 ? count++ : null;
  }
  return count;
}

function checkScholarship(subj) {
  const totalAmount = Object.keys(subj).length;
  let sum = 0;

  for (let grades in subj) {
    sum += subj[grades];
  }
  return sum / totalAmount;
}
