//// Створити поле 30*30 з білих клітинок за допомогою елемента `````.
//// При натисканні на білу клітинку вона повинна змінювати колір на чорний. При натисканні на чорну клітинку вона повинна змінювати колір назад на білий.
//// Сама таблиця повинна бути не вставлена у вихідний HTML-код, а згенерована і додана в DOM сторінки за допомогою Javascript.
//// Обработчик события click нужно повесить на всю таблицу. События всплывают от элемента вверх по DOM дереву, их все можно ловить с помощью одного обработчика событий на таблице, и в нем определять, на какую из ячеек нажал пользователь.
//// При клике на любое место документа вне таблицы, все цвета клеточек должны поменяться на противоположные (подсказка: нужно поставить Event Listener на <body>).
//// Чтобы поменять цвета всех клеточек сразу, не нужно обходить их в цикле. Если помечать нажатые клетки определенным классом, то перекрасить их все одновременно можно одним действием - поменяв класс на самой таблице.

createTable(30, 30);

function createTable(x, y) {
  const cellWidth = 30;

  const tbl = document.createElement('table');
  const tblBody = document.createElement('tbody');
  tbl.style.backgroundColor = 'darkgrey';

  for (let i = 0; i < x; i++) {
    createRow(tblBody, y, cellWidth, i);
  }

  tbl.addEventListener('click', (e) => {
    if (e.target.dataset.hasOwnProperty('id')) {
      changeColor(e.target, 'unselected', 'selected');
    }
  });

  tbl.append(tblBody);
  document.body.append(tbl);

  setupBody('table');
  createStyles();
}

function createRow(parent, cellsAmount, cellDim, indexX) {
  const row = document.createElement('tr');

  for (let j = 0; j < cellsAmount; j++) {
    createCell(row, cellDim, indexX, j);
  }

  parent.append(row);
}
function createCell(parent, cellDim, indexX, indexY) {
  const cell = document.createElement('td');
  cell.classList.add('selected');
  cell.dataset.id = `${indexX}-${indexY}`;
  cell.style.height = `${cellDim}px`;
  cell.style.width = `${cellDim}px`;
  parent.append(cell);
}

function createStyles() {
  const style = document.createElement('style');
  style.innerHTML = `:root {--background-color: #fff; --background-color-selected: #000;} .inverted{--background-color: #000; --background-color-selected: #fff;} table { margin:auto; } .selected { background-color: var(--background-color); } .unselected {background-color: var(--background-color-selected);}`;
  document.getElementsByTagName('head')[0].append(style);
}

function setupBody(element) {
  document.body.style.backgroundColor = 'lightgrey';

  document.body.addEventListener('click', (e) => {
    if (!e.target.closest(element))
      document.querySelector(element).classList.toggle('inverted');
  });
}

function changeColor(target, mainColor, secondaryColor) {
  return target.classList.contains(mainColor)
    ? target.classList.replace(mainColor, secondaryColor)
    : target.classList.replace(secondaryColor, mainColor);
}
