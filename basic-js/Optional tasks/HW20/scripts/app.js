// Реалізувати універсальний фільтр масиву об'єктів.

// Технічні вимоги:
// Написати функцію filterCollection(), яка дозволить відфільтрувати будь-який масив за заданими ключовими словами.
// Функція має приймати на вхід три основні аргументи:
// масив, який треба відфільтрувати
// Рядок з ключовими словами, які треба знайти всередині масиву (одне слово або кілька слів, розділених пробілом)
// boolean прапор, який буде говорити, чи треба знайти всі ключові слова (true), або достатньо збігу одного з них (false)
// четвертий і наступні аргументи будуть іменами полів, у яких треба шукати збіг. Якщо поле не на першому рівні об'єкта, до нього треба вказати повний шлях через .. Рівень вкладеності полів може бути будь-яким.

// Приклад виклику функції:
// filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description')

// В даному прикладі буде відфільтрований масив vehicles, за допомогою ключових слів en_US та Toyota. true у третьому параметрі означає, що для успішного включення запису у фінальний результат має бути збіг за обома ключовими словами. Останні кілька параметрів містять імена полів, у яких треба шукати ключові слова. Наприклад contentType.name означає, що всередині кожного об'єкта vehicle може бути поле contentType, яке є об'єктом або масивом об'єктів, усередині яких може бути поле name. Именно в этом поле (а также в других указанных) необходимо искать сопадения с ключевыми словами.
// У прикладі вище - запис locales.name означає, що поле locales всередині об'єкта vehicle може бути як об'єктом, так і масивом. Якщо воно є масивом, це означає, що всередині масиву знаходяться об'єкти, у кожного з яких може бути властивість name. Для успішної фільтрації достатньо знаходження ключового слова хоча б у одному з елементів масиву.
// Різні ключові слова можуть бути в різних властивостях об'єкта. Наприклад, у прикладі вище, ключове слово en_US може бути знайдено в полі locales.name, тоді як ключове слово Toyota може, наприклад, знаходитися всередині властивості description. При цьому такий об'єкт має бути знайдено.
//! Пошук має бути нечутливим до регістру.

const vehicles = [
  {
    name: 'Toyota',
    description: 'toyota_description',
    contentType: { name: 'content_type' },
    'useless key': 'useless value',
    locales: {
      name: ['en_US', 'it-IT', 'pl', 'uk', 'fr-FR'],
      description: 'description_text',
    },
  },
  {
    name: 'BMW',
    description: 'bmw_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['en_US', 'it-IT', 'pl', 'uk', 'fr-FR'],
      description: 'description_text',
    },
    'useless key': 'useless value',
  },
  {
    name: 'Mercedes',
    description: 'mercedes_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['it-IT', 'uk', 'fr-FR'],
      description: 'description_text',
    },
  },
  {
    name: 'Volvo',
    description: 'volvo_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['it-IT', 'pl', 'uk', 'fr-FR'],
      description: 'description_text',
    },
  },
  {
    name: 'Toyota',
    description: 'toyota_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['it-IT', 'pl', 'uk', 'fr-FR'],
      description: 'description_text',
    },
  },
  {
    name: 'Toyota',
    description: 'toyota_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['en_US', 'fr-FR'],
      description: 'description_text',
    },
  },
  {
    name: 'Jeep',
    description: 'jeep_description',
    contentType: { name: 'content_type' },
    locales: {
      name: ['en_US', 'it-IT', 'pl', 'uk', 'fr-FR'],
      description: 'description_text',
    },
  },
];

const filtered = filterCollection(
  vehicles,
  'en_us toyota',
  true,
  'name',
  'description',
  'contentType.name',
  'locales.name',
  'locales.description'
);

console.log(filtered);

function filterCollection(array, keyWords, condition, ...args) {
  return array.filter((object) => {
    const valuesOfObject = [];

    for (const argument of args) {
      const value = getInternalValues(object, argument);

      if (Array.isArray(value)) {
        for (const item of value) {
          valuesOfObject.push(item.toLowerCase());
        }
      } else {
        valuesOfObject.push(value.toLowerCase());
      }
    }

    const findWords = keyWords.split(' ').map((word) => {
      if (valuesOfObject.includes(word.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    });

    const checkCondition = condition
      ? findWords.every((answer) => answer)
      : findWords.some((answer) => answer);

    if (checkCondition) {
      return object;
    }
  });
}

function getInternalValues(object, argument) {
  if (argument.includes('.')) {
    const argArray = argument.split('.');
    return getInternalValues(checkKey(object, argArray[0]), argArray[1]);
  } else {
    return checkKey(object, argument);
  }
}

function checkKey(object, key) {
  if (object[key]) {
    return object[key];
  } else {
    throw new Error(
      `${object[key]} is empty or key '${key}' is not present in this object`
    );
  }
}
