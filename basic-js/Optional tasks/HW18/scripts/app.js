// Реализовать функцию полного клонирования объекта.

// Технические требования:
// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
//! В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.

const object = {
  number: 0,
  array: [1, 2, 3],
  innerObject: {
    number: 0,
    array: [1, 2, 3],
    innerObject: {
      array: [1, 2, 3],
      number: 0,
    },
  },
};

//! TEST
const newObject = cloneObject(object);

object.number = 1;
object.innerObject.number = 1;
object.innerObject.innerObject.number = 1;
object.array = [4, 5, 6];
object.innerObject.array = [4, 5, 6];
object.innerObject.innerObject.array = [4, 5, 6];

console.log(object);
console.log(newObject);

function cloneObject(obj) {
  let clone = {};
  for (let key in obj) {
    if (Array.isArray(obj[key])) {
      clone[key] = obj[key].splice(0);
    } else if (typeof obj[key] === 'object') {
      clone[key] = cloneObject(obj[key]);
    } else {
      clone[key] = obj[key];
    }
  }
  return clone;
}
