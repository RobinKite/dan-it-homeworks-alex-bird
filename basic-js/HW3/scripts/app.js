function findMultipleOf() {
    const maxRange = getNumber();

    for (let i = 0; i <= maxRange; i++) {
        if (maxRange < 5) {
            console.log('Sorry, no numbers')
        } else if (i%5 === 0 && i !== 0) {
            console.log(i);            
        }
    }

    function getNumber() {
        let userNumber = prompt('Please, enter your number');
    
        while (true) {
            if (parseFloat(userNumber) === parseInt(userNumber)) {
                break;
            } else {
                console.warn(`${userNumber} is not integer`);
                userNumber = prompt('Please, enter your number');
            }
        }
        return userNumber;
    }
}

function findPrimeNumb() {
    let numbArr = [];
    let isPrimeNum = true;
    const arr = getRange();
    const m = arr[0];
    const n = arr[1];
        
    for (let j = m; j <= n; j++) {
        isPrimeNum = true;

        if (j >=2) {
            for (let i = 2; i <= j; i++) {    
                if ((j % i == 0) && (j/i != 1)) {
                    isPrimeNum = false;
                    break;
                }            
            }
            if (isPrimeNum) {
                numbArr.push(j);
            }
        }
    }
    if (numbArr.length > 0) {
        console.log(`In a range from ${m} to ${n} prime numbers are: ${numbArr}`);
    } else {
        console.log(`In a range from ${m} to ${n} prime numbers are not found.`);            
    }

    function checkRange(m,n){
        if ((n >= 2) && (m <= n) && (m >= 0)) {
            return true;
        } else if ((m <= n) && (n === 1) && (m >= 0)) {
            console.warn('1 is not a prime number');
            return false;
        } else if (!m || !n){
            console.warn(`Numbers ${m} and ${n} are incorrect`);
            return false;
        } else {
            console.warn(`Range from ${m} to ${n} is not suitable`);
            return false;
        }
    }

    function getRange() {
        let m = Math.floor(prompt('Please, enter your min range'));
        let n = Math.floor(prompt('Please, enter your max range'));
        if (checkRange(m,n)) {
            return [m,n]
        } else {
            return getRange();
        }        
    }
}


findMultipleOf();

findPrimeNumb();