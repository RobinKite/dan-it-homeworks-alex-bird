// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

export const users = {};
export const posts = {};
export const currentUser = new User({
	id: 11,
	email: 'test@mail.ua',
	name: 'Alex Bird',
	username: 'AlexB',
});

import { createElement } from './createElement.js';
import { Card } from './card.class.js';
import { User } from './user.class.js';
import { openModal, closeModal } from './toggleModal.js';

(async function getList() {
	const responseUsers = await fetch(
		'https://ajax.test-danit.com/api/json/users',
		{ method: 'GET' }
	);
	const responsePosts = await fetch(
		'https://ajax.test-danit.com/api/json/posts',
		{ method: 'GET' }
	);
	const usersData = await responseUsers.json();
	const postsData = await responsePosts.json();
	const fragment = new DocumentFragment();
	const postsContainer = document.querySelector('.posts__container');
	const ul = createElement('ul', 'posts__list');

	const loader = document.querySelector('.loader');

	usersData.forEach((user, i) => {
		const person = new User(user, postsData);

		for (let post of person.posts) {
			const li = createElement('li', 'posts__item');
			const card = new Card(person, post);
			li.append(card.render());
			ul.prepend(li);
		}
	});

	fragment.append(ul);

	loader.remove();

	postsContainer.prepend(fragment);

	const createPostBtn = document.querySelector('.posts__button');
	createPostBtn.addEventListener('click', (e) => {
		openModal('add');
	});
})();
