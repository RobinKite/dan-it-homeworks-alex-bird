import { showError, hideError } from './toggleError.js';
import { currentUser } from './app.js';

export function testFormData(dataObj) {
	if (hasEmptyTextContent(dataObj)) {
		showError();
		throw new Error('Enter text to post!');
	}
	hideError();

	if (hasEmptyValue(dataObj)) {
		setDefaultValues(dataObj);
	}
}

function setDefaultValues(obj, defaults = currentUser) {
	for (const [key, value] of Object.entries(obj)) {
		if (value === null || value === undefined || value === '') {
			obj[key] = defaults[key];
		}
	}
}

function hasEmptyValue(obj) {
	for (const key in obj) {
		if (obj.hasOwnProperty(key) && obj[key] === '') {
			return true;
		}
	}
	return false;
}

function hasEmptyTextContent(obj) {
	return obj.title.trim() === '' || obj.text.trim() === '';
}
