export function showError() {
	document.querySelector('.modal__content').classList.add('error');
}

export function hideError() {
	document.querySelector('.modal__content').classList.remove('error');
}
