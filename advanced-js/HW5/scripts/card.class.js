import { createElement } from './createElement.js';
import { openModal, closeModal } from './toggleModal.js';
import { posts } from './app.js';

export class Card {
	constructor({ name, username, email, date }, { id, title, body }) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.title = title;
		this.body = body;
		this.date = date;
		this.buttonsHandler = buttonsHandler.bind(this);
		posts[id] = this;
	}
	render() {
		const defaultSrc = 'https://cdn.onlinewebfonts.com/svg/img_258083.png';
		const container = createElement('div', 'card', null, null, null, this.id);
		const profileImg = createElement(
			'img',
			'card__image',
			null,
			defaultSrc,
			'profile image'
		);
		const cardContent = createElement('div', ['card__content', 'content']);
		const fullName = createElement('span', 'content__fullname', this.name);
		const email = createElement('span', 'content__email', this.email);
		const time = createElement('span', 'content__time', this.date);
		const title = createElement('p', 'content__title', this.title);
		const text = createElement('p', 'content__text', this.body);
		const cardButtons = createElement('div', ['card__buttons', 'buttons']);
		const deleteBtn = createElement(
			'button',
			'buttons__delete',
			`<i class="fas fa-trash"></i>`
		);
		const editBtn = createElement(
			'button',
			'buttons__edit',
			`<i class="fas fa-edit"></i>`
		);

		cardButtons.addEventListener('click', this.buttonsHandler);

		cardContent.append(fullName, email, time, title, text);
		cardButtons.append(editBtn, deleteBtn);
		container.append(profileImg, cardContent, cardButtons);
		return container;
	}
	changeContents({ name, username, email, title, text }) {
		this.name = name;
		this.username = username;
		this.email = email;
		this.title = title;
		this.body = text;
		this.date = new Date().toLocaleTimeString();

		this.updateContents();
	}
	updateContents() {
		const items = Array.from(
			document.getElementById(this.id).querySelector('.content').children
		);

		items[0].textContent = this.name;
		items[1].textContent = this.email;
		items[2].textContent = this.date;
		items[3].textContent = this.title;
		items[4].textContent = this.body;
	}
}

function buttonsHandler(ev) {
	const parent = ev.target.closest('.card');
	const postId = parent.id;

	const btn = ev.target.closest('button');
	if (!btn) return;

	if (btn.classList.contains('buttons__delete')) {
		deletePost(parent, postId);
	}
	if (btn.classList.contains('buttons__edit')) {
		openModal('edit', postId);
	}
}

async function deletePost(parent, postId) {
	if (postId > 100) {
		console.warn(
			'Currently able to delete only posts from 1 to 100. This time trying to delete 100th post.'
		);
	}

	const response = await fetch(
		`https://ajax.test-danit.com/api/json/posts/${postId > 100 ? 100 : postId}`,
		{
			method: 'DELETE',
		}
	);
	if (response.ok) {
		parent.closest('.posts__item').remove();
		delete posts[postId];
	} else {
		throw new Error('Failed to delete post from server. Try again.');
	}
}

export async function editPost(data) {
	if (data.id > 100) {
		console.warn(
			'Currently able to edit only posts from 1 to 100. This time trying to edit 100th post.'
		);
	}

	const response = await fetch(
		`https://ajax.test-danit.com/api/json/posts/${
			data.id > 100 ? 100 : data.id
		}`,
		{
			method: 'PUT',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);
	if (response.ok) {
		const responseContent = await response.json();
		posts[data.id].changeContents(responseContent);
		closeModal();
	} else {
		throw new Error(`Error ${response.status}. Try again.`);
	}
}
