import { posts } from './app.js';
import { showError, hideError } from './toggleError.js';
import { form } from './form.action.js';

export function openModal(text, postId = null) {
	const modal = document.getElementById('myModal');
	const closeBtn = document.querySelector('.modal__close');
	const submitBtn = document.querySelector('.modal__submit');

	submitBtn.textContent = `${capitalizeFirstLetter(text)} post`;
	submitBtn.dataset.id = text;

	if (text === 'edit') {
		const { id, name, username, email, title, body } = posts[postId];

		form.id.value = id;
		form.name.value = name;
		form.username.value = username;
		form.email.value = email;
		form.title.value = title;
		form.text.value = body;
	}

	modal.classList.add('opened');
	document.body.style.overflow = 'hidden';

	closeBtn.addEventListener('click', closeModal);
}

export function closeModal() {
	const modal = document.getElementById('myModal');
	const closeBtn = document.querySelector('.modal__close');
	const inputs = document.querySelectorAll('.modal__input');
	const submitBtn = document.querySelector('.modal__submit');

	submitBtn.dataset.id = '';
	modal.classList.remove('opened');
	document.body.style.overflow = '';

	hideError();

	closeBtn.removeEventListener('click', closeModal);

	setTimeout(() => inputs.forEach((input) => (input.value = '')), 0);
}

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
