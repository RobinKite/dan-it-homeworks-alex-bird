import { users } from './app.js';

export class User {
	constructor({ id, name, username, email }, postsArr) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.date = new Date().toLocaleTimeString();
		this.posts = postsArr ? this.getPosts(postsArr) : [];
		users[id] = this;
	}
	getPosts(arr) {
		return arr.filter(({ userId }) => userId === this.id);
	}
	addPost(post) {
		this.posts.push(post);
	}
}
