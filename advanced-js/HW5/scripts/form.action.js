import { showError, hideError } from './toggleError.js';
import { testFormData } from './testData.js';
import { Card, editPost } from './card.class.js';
import { createElement } from './createElement.js';
import { users } from './app.js';
import { closeModal } from './toggleModal.js';

export const form = document.forms[0];
form.addEventListener('submit', (e) => {
	e.preventDefault();

	formHandler(form);
});

export function formHandler(form) {
	const formData = new FormData(form);
	const formDataObj = Object.fromEntries(formData.entries());

	testFormData(formDataObj);

	const formMethod = document.querySelector('.modal__submit').dataset.id;

	if (formMethod === 'add') {
		createPost(formDataObj);
	}
	if (formMethod === 'edit') {
		editPost(formDataObj);
	}
}

async function createPost(dataObj) {
	const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
		method: 'POST',
		body: JSON.stringify(dataObj),
		headers: {
			'Content-Type': 'application/json',
		},
	});
	// const responseJSON = await response.json();
	if (response.ok) {
		const ul = document.querySelector('.posts__list');
		const li = createElement('li', 'posts__item');

		const { title, text: body } = dataObj;
		const id = parseInt(dataObj.id);

		const postId = document.querySelectorAll('.posts__item').length + 1;

		const user = users[id];

		user.addPost({ title, body, id: postId, userId: id });

		const card = new Card(user, user.posts[user.posts.length - 1]);

		li.append(card.render());
		ul.prepend(li);

		closeModal();
	} else {
		throw new Error(`Error ${response.status}. Try again.`);
	}
}
