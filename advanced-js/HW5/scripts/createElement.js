export function createElement(
	tagName,
	className,
	textContent,
	src,
	alt = 'image',
	id
) {
	const element = document.createElement(tagName);
	if (className) {
		if (typeof className === 'string') {
			element.classList.add(className);
		} else if (Array.isArray(className)) {
			element.classList.add(...className);
		}
	}
	if (textContent) {
		textContent.includes('<')
			? (element.innerHTML = textContent)
			: (element.textContent = textContent);
	}
	if (src) {
		element.src = src;
		element.alt = alt;
	}
	if (id) {
		element.id = id;
	}
	return element;
}
