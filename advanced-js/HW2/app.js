// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
	{
		author: 'Люсі Фолі',
		name: 'Список запрошених',
		price: 70,
	},
	{
		author: 'Сюзанна Кларк',
		name: 'Джонатан Стрейндж і м-р Норрелл',
	},
	{
		name: 'Дизайн. Книга для недизайнерів.',
		price: 70,
	},
	{
		author: 'Алан Мур',
		name: 'Неономікон',
		price: 70,
	},
	{
		author: 'Террі Пратчетт',
		name: 'Рухомі картинки',
		price: 40,
	},
	{
		author: 'Анґус Гайленд',
		name: 'Коти в мистецтві',
	},
];

function generateElement(tag, className, content) {
	const element = document.createElement(tag);
	element.classList.add(className);
	if (content) element.textContent = content;
	return element;
}

(function checkObject(arr) {
	const div = document.getElementById('root');
	const ul = generateElement('ul', 'root__list');
	const keys = ['author', 'name', 'price'];

	arr.forEach((obj) => {
		try {
			for (let key of keys) {
				if (!Object.keys(obj).includes(key))
					throw new Error(
						`There is no ${key} in this object`
					);
			}
			const li = generateElement('li', 'root__item');
			for (let key in obj) {
				const p = generateElement(
					'p',
					'root__text',
					`${key[0].toUpperCase() + key.slice(1)}: ${
						obj[key]
					}`
				);
				li.append(p);
			}
			ul.append(li);
		} catch (error) {
			console.log(error);
		}
	});

	div.append(ul);
})(books);
