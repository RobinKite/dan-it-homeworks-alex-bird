// Створити адаптований аналог гри Whack a mole. (https://gitlab.com/dan-it/groups/pe-15-online/-/raw/main/advanced-js/homework/homework7_optional-whack-a-mole/moles.png)
// Створити поле 10*10 за допомогою елемента <table>.
// Суть гри: будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір. Користувач повинен протягом відведеного часу встигнути клацнути на зафарбовану комірку. Якщо користувач встиг це зробити, вона забарвлюється зелений колір, користувач отримує 1 очко. Якщо не встиг – вона забарвлюється у червоний колір, комп'ютер отримує 1 очко.
// Гра триває доти, доки половина комірок на полі не будуть пофарбовані у зелений чи червоний колір. Як тільки це станеться, той гравець (людина чи комп'ютер), чиїх комірок на полі більше, перемагає.
// Гра повинна мати три рівні складності, що вибираються перед стартом гри:
// Легкий – нова комірка підсвічується кожні 1.5 секунди;
// Середній - нова комірка підсвічується раз на секунду;
// Важкий - нова комірка підсвічується кожні півсекунди.
// Після закінчення гри вивести на екран повідомлення про те, хто переміг.
// Після закінчення гри має бути можливість змінити рівень складності та розпочати нову гру.
// Використовувати функціонал ООП під час написання програми.
// За бажанням, замість фарбування комірок кольором, можна вставляти туди картинки.

Array.prototype.shuffle = function () {
	let i = this.length,
		j;
	while (--i > 0) {
		j = Math.floor(Math.random() * (i + 1));
		[this[j], this[i]] = [this[i], this[j]];
	}
	return this;
};

const HEART = '❤️';
const DEATH = '💀';
const HIGHSCORE = [
	{
		score: 100,
		name: 'John',
		mode: 'timer',
		lives: 1,
		difficulty: 'easy',
	},
	{
		score: 80,
		name: 'Alex',
		mode: 'timer',
		lives: 2,
		difficulty: 'easy',
	},
	{
		score: 60,
		name: 'Kate',
		mode: 'timer',
		lives: 1,
		difficulty: 'easy',
	},
	{
		score: 40,
		name: 'Bill',
		mode: 'timer',
		lives: 3,
		difficulty: 'easy',
	},
	{
		score: 20,
		name: 'Jack',
		mode: 'timer',
		lives: 1,
		difficulty: 'easy',
	},
	{
		score: 0,
		name: 'Luke',
		mode: 'timer',
		lives: 5,
		difficulty: 'easy',
	},
	{
		score: 100,
		name: 'John',
		mode: 'moles',
		lives: 1,
		difficulty: 'easy',
	},
	{
		score: 80,
		name: 'Alex',
		mode: 'moles',
		lives: 5,
		difficulty: 'easy',
	},
	{
		score: 60,
		name: 'Kate',
		mode: 'moles',
		lives: 2,
		difficulty: 'easy',
	},
	{
		score: 40,
		name: 'Bill',
		mode: 'moles',
		lives: 3,
		difficulty: 'easy',
	},
	{
		score: 20,
		name: 'Jack',
		mode: 'moles',
		lives: 1,
		difficulty: 'easy',
	},
	{
		score: 0,
		name: 'Luke',
		mode: 'moles',
		lives: 1,
		difficulty: 'easy',
	},
];

// class Mole {}

class Timer {
	placeholder = document.querySelector('.field__timer');
	minutes = document.querySelector('[data-minutes]');
	seconds = document.querySelector('[data-seconds]');

	static mainTimer = [];
	static isRunning = false;

	constructor(time, game) {
		this.time = time;
		this.intervalfunc = this.intervalFunc.bind(this);

		this.minutes.textContent = stringifyTime(
			Math.floor(this.time / 60)
		);
		this.seconds.textContent = stringifyTime(
			Math.floor(this.time % 60)
		);

		this.game = game;
	}
	startTimer() {
		if (Timer.isRunning) {
			this.killTimer();
		}
		this.placeholder.classList.remove('disable-timer');
		Timer.mainTimer.push(
			setInterval(this.intervalfunc, 1000)
		);
	}
	killTimer() {
		Timer.mainTimer.forEach((t) => clearInterval(t));
		Timer.mainTimer = [];
		Timer.isRunning = false;
		this.placeholder.classList.add('disable-timer');
	}
	intervalFunc() {
		Timer.isRunning = true;
		this.time--;
		this.seconds.textContent = stringifyTime(
			this.time % 60
		);
		if (
			this.time % 60 === 59 &&
			this.minutes.textContent != 0
		) {
			this.minutes.textContent = stringifyTime(
				this.minutes.textContent - 1
			);
		}
		if (this.time <= 0) {
			this.game.stopGame();
			this.killTimer();
		}
	}
}

class Hole {
	constructor(coords = [0, 0], molesAmount = 1, game) {
		this._coords = coords;
		this._isClosed = false;
		this._hasMole = false;
		this.molesAmount = molesAmount;
		this.game = game;
		this.handleclick = this.handleClick.bind(this);
	}

	get coords() {
		return this._coords;
	}
	set coords(value) {
		this._coords = value;
	}
	get isClosed() {
		return this._isClosed;
	}
	set isClosed(value) {
		this._isClosed = value;
	}
	get hasMole() {
		return this._hasMole;
	}
	set hasMole(value) {
		this._hasMole = value;
	}

	checkHole() {
		return !this._isClosed;
	}
	closeHole() {
		this.hasMole = false;
		this._isClosed = true;
		this.cell.classList.remove('mole');
		this.molesAmount = 0;
		this.cell.textContent = '';
		this.cell.classList.add('closed');
		this.cell.removeEventListener(
			'click',
			this.handleclick
		);
	}
	handleClick(ev) {
		if (this.game.gamemode === 'moles') {
			this.molesAmount--;

			this.cell.textContent = this.molesAmount;
		}
		if (this.checkHole() && this._hasMole) {
			this.highlightHole(ev.target);
			this.game.updateScore(1);
			this.hasMole = false;
			clearTimeout(this.game.moleTimer);
		} else {
			this.game.updateScore(-1);
			this.game.lives--;
			this.game.livesElement.textContent = HEART.repeat(
				this.game.lives
			);
			if (this.game.lives === 0) {
				this.game.livesElement.textContent = DEATH;
				this.game.stopGame();
			}
		}
		if (!this.molesAmount) {
			this.game.removeHole(this);
		}
	}
	highlightHole(hole) {
		this.cell.classList.remove('mole');
		hole.classList.add('highlight');
		setTimeout(() => {
			hole.classList.remove('highlight');
		}, 100);
	}
}

class Game {
	#holesArray = [];

	mapDifficultyToDelay = {
		hard: 0.5,
		medium: 1,
		easy: 1.5,
	};

	gameField = document.querySelector('.game');
	fieldBody = document.querySelector('.field__body');
	highscore = document.querySelector('.highscore');
	livesElement = document.querySelector('.field__lives');
	scoreElement = document.querySelector(
		'.highscore__current'
	);

	static isRunning = false;
	static showMoleInterval = [];

	constructor(data) {
		const dataArr = data.split(' ');

		this._gamemode = dataArr[0];
		this._difficulty = dataArr[1];
		this._size = dataArr[2];
		this._molesAmount =
			this.gamemode === 'moles'
				? dataArr[3]
				: this.countMolesForTimerMode(dataArr[3]);
		this._mainTime =
			this.gamemode === 'timer' ? dataArr[3] : null;
		this._lives = dataArr[4];

		this._score = 0;
		this.moleTimer = null;
		this.gameTimer = new Timer(this.mainTime, this);

		this.holesSequence = pushNumbers(
			this.molesAmount,
			this.size
		).shuffle();
		this._currentIteration = 0;
	}

	get gamemode() {
		return this._gamemode;
	}
	set gamemode(value) {
		this._gamemode = value;
	}
	get difficulty() {
		return this._difficulty;
	}
	set difficulty(value) {
		this._difficulty = value;
	}
	get size() {
		return this._size;
	}
	set size(value) {
		this._size = value;
	}
	get molesAmount() {
		return this._molesAmount;
	}
	set molesAmount(value) {
		this._molesAmount = value;
	}
	get lives() {
		return this._lives;
	}
	set lives(value) {
		this._lives = value;
	}
	get mainTime() {
		return this._mainTime;
	}
	set mainTime(value) {
		this._mainTime = value;
	}
	get score() {
		return this._score;
	}
	set score(value) {
		this._score = value;
	}
	get currentIteration() {
		return this._currentIteration;
	}
	set currentIteration(value) {
		this._currentIteration = value;
	}

	countMolesForTimerMode(value) {
		return (
			(value *
				this.mapDifficultyToDelay[this.difficulty]) /
				9 +
			1
		);
	}
	startGame() {
		if (
			Game.isRunning ||
			Array.from(this.fieldBody.children).length
		) {
			this.clearField();
		}

		if (this.gamemode === 'timer') {
			this.gameTimer.startTimer();
		}

		this.gameField.classList.remove('hidden');
		this.highscore.classList.remove('hidden');

		this.startShowingMoles(this.difficulty);
		this.createField(this.size);
	}
	clearField() {
		this.fieldBody.innerHTML = '';
		// const arr = Array.from(this.fieldBody.children);
		// arr.forEach((el) => el.remove());
		// this.stopGame();
	}
	stopGame() {
		Game.showMoleInterval.forEach((t) =>
			clearInterval(t)
		);
		Game.showMoleInterval = [];
		clearTimeout(this.moleTimer);

		for (let el of this.#holesArray) {
			this.removeHole(el);
		}

		if (Timer.isRunning) this.gameTimer.killTimer();

		if (Game.isRunning) {
			console.group('Game Over');
			console.log(`Your score is ${this.score}.`);
			console.log(`You have ${this.lives} lives left.`);
			console.groupEnd();

			Game.isRunning = false;
		}

		this.saveRecord();
		this.changeLeaderboard();
	}
	changeLeaderboard() {
		createHighscoreTable(HIGHSCORE, this.gamemode);
	}
	saveRecord() {
		const currentValue = localStorage.getItem('records')
			? localStorage.getItem('records')
			: null;

		const myArray = localStorage.getItem('records')
			? JSON.parse(currentValue)
			: [];

		const newObj = {
			score: this.score,
			name: 'Player',
			mode: this.gamemode,
			lives: this.lives,
			difficulty: this.difficulty,
		};

		myArray.push(newObj);

		const newValue = JSON.stringify(myArray);

		localStorage.setItem('records', newValue);
	}
	startShowingMoles() {
		this.scoreElement.textContent = `Current score: ${this.score}`;

		Game.isRunning = true;

		Game.showMoleInterval.push(
			setInterval(() => {
				this.currentIteration !==
				this.holesSequence.length
					? this.showMole()
					: this.stopGame();
			}, this.mapDifficultyToDelay[this.difficulty] * 1000)
		);
	}
	createField(size) {
		for (let i = 0; i < size; i++) {
			const row = createElement(
				this.fieldBody,
				'tr',
				'field__row'
			);
			for (let j = 0; j < size; j++) {
				const hole = new Hole(
					[i, j],
					this.molesAmount,
					this
				);
				hole.cell = createElement(
					row,
					'td',
					'field__cell',
					this.gamemode === 'moles'
						? hole.molesAmount
						: null,
					'click',
					hole.handleclick
				);
				this.addHole(hole);
			}
		}
		this.livesElement.textContent = HEART.repeat(
			this.lives
		);
	}
	showMole() {
		let r = this.holesSequence[this.currentIteration];

		try {
			while (
				this.#holesArray[r].isClosed &&
				this.currentIteration <
					this.holesSequence.length
			) {
				this.currentIteration++;
				r = this.holesSequence[this.currentIteration];
			}

			this.#holesArray[r].cell.classList.add('mole');
			this.#holesArray[r].hasMole = true;

			this.moleTimer = setTimeout(() => {
				if (this.gamemode === 'moles') {
					this.#holesArray[r].molesAmount--;

					this.#holesArray[r].cell.textContent =
						this.#holesArray[r].molesAmount;

					if (this.#holesArray[r].molesAmount === 0) {
						this.removeHole(this.#holesArray[r]);
					}
				}

				if (
					r !==
					this.holesSequence[this.currentIteration - 1]
				) {
					this.#holesArray[r].cell.classList.remove(
						'mole'
					);
				}
			}, this.mapDifficultyToDelay[this.difficulty] * 1000);

			if (
				this.currentIteration <
				this.holesSequence.length
			) {
				this.currentIteration++;
			}
		} catch (e) {
			console.log(e);
		}
	}

	updateScore(value) {
		this.score += value;
		this.scoreElement.textContent = `Current score: ${this.score}`;
	}
	addHole(hole) {
		this.#holesArray.push(hole);
	}
	removeHole(hole) {
		hole.closeHole();
	}
}

(function () {
	const form = document.querySelector('form');
	const [timerField, molesField] = [
		document.querySelector('[data-moles]'),
		document.querySelector('[data-timer]'),
	];
	const [timerMode, molesMode] = [
		document.querySelector('#timer-mode'),
		document.querySelector('#moles-mode'),
	];

	form.addEventListener('change', () => {
		if (timerMode.checked) {
			timerField.disabled = true;
			molesField.disabled = false;
		} else if (molesMode.checked) {
			timerField.disabled = false;
			molesField.disabled = true;
		}
	});

	form.addEventListener(
		'submit',
		(event) => {
			event.preventDefault();

			const data = new FormData(form);

			let output = '';

			for (const entry of data) {
				output += `${entry[1]} `;
			}

			createHighscoreTable(
				HIGHSCORE,
				output.split(' ')[0]
			);

			const game = new Game(output);
			game.startGame();
		},
		false
	);
})();

function createHighscoreTable(data, mode) {
	const body = document.querySelector('.highscore__body');

	body.innerHTML = '';
	try {
		if (localStorage.getItem('records')) {
			const arr = JSON.parse(
				localStorage.getItem('records')
			);

			data = data.concat(arr);
		}
	} catch (e) {
		console.log(e);
	}
	data = data.filter((el) => el.mode === mode);
	data.sort((a, b) => b.score - a.score);
	for (const scoreData of data) {
		const { score, name, mode, lives, difficulty } =
			scoreData;
		const row = createElement(body, 'tr');
		const rankCell = createElement(
			row,
			'td',
			null,
			parseInt(data.indexOf(scoreData) + 1)
		);
		const nameCell = createElement(row, 'td', null, name);
		const scoreCell = createElement(
			row,
			'td',
			null,
			score + ''
		);
		const livesCell = createElement(
			row,
			'td',
			null,
			lives + ''
		);
		const diffCell = createElement(
			row,
			'td',
			null,
			difficulty
		);
		const modeCell = createElement(row, 'td', null, mode);
	}
}

function pushNumbers(m, n) {
	const result = [];
	const maxNumber = n * n;
	for (let i = 0; i < m; i++) {
		for (let j = 0; j < maxNumber; j++) {
			result.push(j);
		}
	}
	return result;
}

function stringifyTime(value) {
	return String(value).padStart(2, '0');
}

function createElement(
	parent,
	tag,
	className,
	textContent,
	event,
	handle
) {
	const elem = document.createElement(tag);
	if (className) elem.classList.add(className);
	if (textContent) elem.textContent = textContent;
	if (parent) parent.append(elem);

	if (event && handle)
		elem.addEventListener(event, handle);
	return elem;
}
