class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}
	get name() {
		return this._name;
	}
	set name(value) {
		this._name = value;
	}
	get age() {
		return this._age;
	}
	set age(value) {
		this._age = value;
	}
	get salary() {
		return this._salary;
	}
	set salary(value) {
		this._salary = value;
	}
}
class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	get salary() {
		return super.salary * 3;
	}
}
const junior = new Programmer('Alex', 24, 1000, 'js');
const senior = new Programmer('Max', 28, 3000, 'js');
const middle = new Programmer('John', 26, 2000, 'js');
console.log(junior, junior.salary);
console.log(senior, senior.salary);
console.log(middle, middle.salary);
