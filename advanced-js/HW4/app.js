//* Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

//? Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
//* Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
//* Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

//? Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

const moviesName = 'Star Wars';

fetch('https://ajax.test-danit.com/api/swapi/films', {
	method: 'GET',
})
	.then((data) => data.json())
	.then((data) => {
		for (let obj of data) {
			const {
				characters,
				episodeId,
				name,
				openingCrawl,
			} = obj;
			const div = createDOMElement('div', 'wrapper');
			const h2 = createDOMElement(
				'h2',
				'wrapper__title',
				`${moviesName} ${episodeId}: ${name}`
			);
			const p = createDOMElement(
				'p',
				'wrapper__description',
				openingCrawl
			);
			const ul = createDOMElement('ul', 'wrapper__list');
			const loader = createDOMElement('div', 'loader');

			const promise = characters.map((character) => {
				return fetch(character, { method: 'GET' })
					.then((data) => data.json())
					.then(({ name: charName }) => {
						const li = createDOMElement(
							'li',
							'wrapper__item',
							charName
						);
						return li;
					});
			});

			Promise.all(promise).then((lis) => {
				ul.append(...lis);
				loader.remove();
			});

			div.append(h2, p, loader, ul);
			document.body.append(div);
		}
	});

function createDOMElement(tagName, className, textContent) {
	const element = document.createElement(tagName);
	element.classList.add(className);
	element.textContent = textContent;
	return element;
}
