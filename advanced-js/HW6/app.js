// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.querySelector('.btn');
btn.addEventListener('click', btnHandler);

const textContainers = document.querySelectorAll('[data-value]');
const loader = document.querySelector('.loader');
const warningContainer = document.querySelector('.warnings');

async function btnHandler(ev) {
	showLoader();

	const responseIP = await fetch('https://api.ipify.org/?format=json', {
		method: 'GET',
	});
	if (responseIP.ok) {
		hideLoader();
		clearWarnings();

		const { ip } = await responseIP.json();

		showLoader();

		const responseFullInfo = await fetch(
			`http://ip-api.com/json/${ip}?fields=status,message,continent,country,region,regionName,city,district,zip`
		);
		const { continent, country, region, regionName, city, district, zip } =
			await responseFullInfo.json();

		hideLoader();

		const data = { continent, country, region, regionName, city, district };
		const spareData = { district: { zip }, regionName: { region } };

		textContainers.forEach((el) => {
			const searchingData = el.dataset.value;

			if (data[searchingData]) {
				el.textContent = `${searchingData}: ${data[searchingData]}`;
			} else {
				const spareDataKey = Object.keys(spareData[searchingData])[0];
				const spareDataValue = Object.values(spareData[searchingData])[0];
				const warningText = `* - warning: '${searchingData}' value is empty, so taking '${spareDataKey}' instead.`;

				el.textContent = `${spareDataKey}: ${spareDataValue}`;

				addWarning(warningText, el);
			}
		});
	}
}

function clearWarnings() {
	warningContainer.innerHTML = '';
}

function addWarning(text, element) {
	const warningElem = createElement('p', 'warning', text);
	const warningStar = createElement('span', 'warning__symbol', '*');

	element.insertAdjacentElement('afterbegin', warningStar);
	warningContainer.append(warningElem);
}

function createElement(tagName, className, textContent) {
	const elem = document.createElement(tagName);
	elem.classList.add(className);
	elem.textContent = textContent;
	return elem;
}

function showLoader() {
	loader.classList.remove('hidden');
}

function hideLoader() {
	loader.classList.add('hidden');
}
